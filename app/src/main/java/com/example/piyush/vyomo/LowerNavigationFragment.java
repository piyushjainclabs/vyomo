package com.example.piyush.vyomo;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


public class LowerNavigationFragment extends Fragment {
    ImageButton imageButtonDashboard, imageButtonAppointment, imageButtonCalendar, imageButtonUser, imageButtonMenu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lower_navigation_fragment, container, false);
        imageButtonDashboard = (ImageButton) view.findViewById(R.id.dashboard);
        imageButtonAppointment = (ImageButton) view.findViewById(R.id.appointment);
        imageButtonCalendar = (ImageButton) view.findViewById(R.id.calendar);
        imageButtonUser = (ImageButton) view.findViewById(R.id.user);
        imageButtonMenu = (ImageButton) view.findViewById(R.id.menu);

        imageButtonDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageButtonDashboard.setBackgroundColor(Color.parseColor("#BB1A35"));
                imageButtonAppointment.setBackgroundColor(0);
                imageButtonCalendar.setBackgroundColor(0);
                imageButtonUser.setBackgroundColor(0);
                imageButtonMenu.setBackgroundColor(0);
            }
        });

        imageButtonAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageButtonAppointment.setBackgroundColor(Color.parseColor("#BB1A35"));
                imageButtonDashboard.setBackgroundColor(0);
                imageButtonCalendar.setBackgroundColor(0);
                imageButtonUser.setBackgroundColor(0);
                imageButtonMenu.setBackgroundColor(0);
            }
        });

        imageButtonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageButtonCalendar.setBackgroundColor(Color.parseColor("#BB1A35"));
                imageButtonAppointment.setBackgroundColor(0);
                imageButtonDashboard.setBackgroundColor(0);
                imageButtonUser.setBackgroundColor(0);
                imageButtonMenu.setBackgroundColor(0);
            }
        });

        imageButtonUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageButtonUser.setBackgroundColor(Color.parseColor("#BB1A35"));
                imageButtonAppointment.setBackgroundColor(0);
                imageButtonCalendar.setBackgroundColor(0);
                imageButtonDashboard.setBackgroundColor(0);
                imageButtonMenu.setBackgroundColor(0);
            }
        });

        imageButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageButtonMenu.setBackgroundColor(Color.parseColor("#BB1A35"));
                imageButtonAppointment.setBackgroundColor(0);
                imageButtonCalendar.setBackgroundColor(0);
                imageButtonUser.setBackgroundColor(0);
                imageButtonDashboard.setBackgroundColor(0);
            }
        });

        return view;
    }
}
