package com.example.piyush.vyomo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SignUp extends ActionBarActivity {
    Spinner typeSpinner;
    RelativeLayout layoutSalonName;
    int SALON_NAME = 100;
    TextView textViewSalonName;
    EditText editTextFirstName, editTextLastName, editTextPhone, editTextEmail, editTextPassword, editTextConfirmPassword;
    Button buttonJoin;
    CheckBox checkBoxTerms;
    int userType = 0; // stylist - 0   Manager - 1
    private String stringFirstName, stringLastName, stringPhone, stringEmail, stringPassword, stringConfirmPassword, stringSalonName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().hide();
        typeSpinner = (Spinner) findViewById(R.id.type_spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.type, R.layout.support_simple_spinner_dropdown_item);
        typeSpinner.setAdapter(spinnerAdapter);
        layoutSalonName = (RelativeLayout) findViewById(R.id.salon_layout);
        layoutSalonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUp.this, SearchSalon.class);
                startActivityForResult(intent, SALON_NAME);
            }
        });
        buttonJoin = (Button) findViewById(R.id.button_join);
        buttonJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateDetails();
            }
        });
        textViewSalonName = (TextView) findViewById(R.id.salon_name);
    }

    private void validateDetails() {
        editTextFirstName = (EditText) findViewById(R.id.first_name);
        editTextLastName = (EditText) findViewById(R.id.last_name);
        editTextPhone = (EditText) findViewById(R.id.phone);
        editTextEmail = (EditText) findViewById(R.id.email);
        editTextPassword = (EditText) findViewById(R.id.password);
        editTextConfirmPassword = (EditText) findViewById(R.id.confirm_password);

        checkBoxTerms = (CheckBox) findViewById(R.id.checkbox);

        stringFirstName = editTextFirstName.getText().toString().trim();
        stringLastName = editTextLastName.getText().toString().trim();
        stringPhone = editTextPhone.getText().toString().trim();
        stringEmail = editTextEmail.getText().toString().trim();
        stringPassword = editTextPassword.getText().toString().trim();
        stringConfirmPassword = editTextConfirmPassword.getText().toString().trim();
        stringSalonName = textViewSalonName.getText().toString().trim();

        userType = typeSpinner.getSelectedItemPosition();

        if (stringFirstName.isEmpty()) {
            editTextFirstName.setText("");
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Enter First Name").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        } else if (stringLastName.isEmpty()) {
            editTextLastName.setText("");
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Enter Last Name").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        } else if (stringPhone.isEmpty()) {
            editTextPhone.setText("");
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Enter Phone No.").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        } else if (!stringEmail.isEmpty()) {
            if (!validate(stringEmail)) {
                editTextEmail.setText("");
                new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Enter Correct Email").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
                return;
            }
        } else if (stringPassword.isEmpty()) {
            editTextPassword.setText("");
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Enter Password").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        } else if (stringConfirmPassword.isEmpty()) {
            editTextEmail.setText("");
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Enter Confirm Password").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        } else if (!stringPassword.equals(stringConfirmPassword)) {
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Sorry The Passwords Do Not Match").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        } else if (!checkBoxTerms.isChecked()) {
            new AlertDialog.Builder(SignUp.this).setTitle("Alert").setMessage("Please Accept the Terms Of Agreement").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        }
        Intent intent = new Intent(SignUp.this, SalonProfile.class);
        startActivity(intent);

    }

    public Boolean validate(final String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SALON_NAME) {
            if (resultCode == RESULT_OK) {
                textViewSalonName.setText(data.getStringExtra("salonName"));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
