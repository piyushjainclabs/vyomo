package com.example.piyush.vyomo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.piyush.vyomo.R;
import com.example.piyush.vyomo.entities.Salon;

import java.util.ArrayList;

public class SalonListAdapter extends BaseAdapter {
    public ArrayList<Salon> salonList;
    Context ctx;
    ImageView imageViewSalonLogo;
    TextView textViewSalonName;

    public SalonListAdapter(Context ctx, ArrayList<Salon> salonList) {
        this.salonList = salonList;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return salonList.size();
    }

    @Override
    public Object getItem(int position) {
        return salonList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.salon_list_item, null);
        imageViewSalonLogo = (ImageView) row.findViewById(R.id.salon_logo);
        textViewSalonName = (TextView) row.findViewById(R.id.salon_name);
        //imageViewSalonLogo.setBackgroundResource(R.drawable.salon_logo_placeholder);
        textViewSalonName.setText(salonList.get(position).getSalonName());
        return row;

    }
}
