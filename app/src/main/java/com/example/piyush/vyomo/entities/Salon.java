package com.example.piyush.vyomo.entities;

/**
 * Created by PIYUSH on 26-02-2015.
 */
public class Salon {
    String salonLogo;
    String salonName;

    public Salon(String salonLogo, String salonName) {
        this.salonLogo = salonLogo;
        this.salonName = salonName;
    }

    public String getSalonLogo() {
        return salonLogo;
    }

    public void setSalonLogo(String salonLogo) {
        this.salonLogo = salonLogo;
    }

    public String getSalonName() {
        return salonName;
    }

    public void setSalonName(String salonName) {
        this.salonName = salonName;
    }
}
