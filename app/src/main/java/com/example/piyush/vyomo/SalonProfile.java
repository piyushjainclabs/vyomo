package com.example.piyush.vyomo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class SalonProfile extends ActionBarActivity implements OnMapReadyCallback {
    ImageButton imageButtonBack, imageButtonShare;
    ViewPager viewPager;
    //String[] image = {"http://sohohair.ca/theme/508aca67dabe9d6ffb004165/stylesheets/images/salon-1-wide.jpg", "http://sohohair.ca/theme/508aca67dabe9d6ffb004165/stylesheets/images/salon-lighting-1.jpg"};
    int[] images = {R.drawable.salon_image1, R.drawable.salon_image2};
    RelativeLayout relativeLayoutCall, relativeLayoutEmail, relativeLayoutBook;
    ImageView imageViewCall, imageViewEmail, imageViewBook;
    LowerNavigationFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_profile);
        getSupportActionBar().hide();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fragment = (LowerNavigationFragment) getFragmentManager().findFragmentById(R.id.lower);

        viewPager = (ViewPager) findViewById(R.id.pager);
        ImagePagerAdapter adapter = new ImagePagerAdapter(this, images);
        viewPager.setAdapter(adapter);

        imageButtonBack = (ImageButton) findViewById(R.id.back);
        imageButtonShare = (ImageButton) findViewById(R.id.share);
        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalonProfile.this, SignUp.class);
                startActivity(intent);
            }
        });
        imageButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/jpeg");
                startActivity(Intent.createChooser(shareIntent, "Share Image"));
            }
        });

        relativeLayoutCall = (RelativeLayout) findViewById(R.id.call_layout);
        relativeLayoutEmail = (RelativeLayout) findViewById(R.id.email_layout);
        relativeLayoutBook = (RelativeLayout) findViewById(R.id.book_layout);

        imageViewCall = (ImageView) findViewById(R.id.call_image);
        imageViewEmail = (ImageView) findViewById(R.id.email_image);
        imageViewBook = (ImageView) findViewById(R.id.book_image);

        relativeLayoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayoutCall.setBackgroundColor(Color.parseColor("#E23351"));
                relativeLayoutEmail.setBackgroundColor(0);
                relativeLayoutBook.setBackgroundColor(0);
                imageViewCall.setImageResource(R.drawable.call_white);
                imageViewEmail.setImageResource(R.drawable.email_red);
                imageViewBook.setImageResource(R.drawable.book_red);
            }
        });
        relativeLayoutEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayoutEmail.setBackgroundColor(Color.parseColor("#E23351"));
                relativeLayoutCall.setBackgroundColor(0);
                relativeLayoutBook.setBackgroundColor(0);
                imageViewCall.setImageResource(R.drawable.call_red);
                imageViewEmail.setImageResource(R.drawable.email_white);
                imageViewBook.setImageResource(R.drawable.book_red);
            }
        });
        relativeLayoutBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayoutBook.setBackgroundColor(Color.parseColor("#E23351"));
                relativeLayoutEmail.setBackgroundColor(0);
                relativeLayoutCall.setBackgroundColor(0);
                imageViewCall.setImageResource(R.drawable.call_red);
                imageViewEmail.setImageResource(R.drawable.email_red);
                imageViewBook.setImageResource(R.drawable.book_white);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_salon_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(-33.867, 151.206);
        googleMap.setMyLocationEnabled(false);
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

        googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_locator)));
    }
}
